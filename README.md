# samurai-wolfram-plugin

A Samurai plugin that provides a command with a hook to the WolframAlpha API. Useful for mathy things and whatever else
people use knowledge engines for.