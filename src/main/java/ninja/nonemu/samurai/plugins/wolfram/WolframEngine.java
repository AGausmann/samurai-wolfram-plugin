package ninja.nonemu.samurai.plugins.wolfram;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.*;

public class WolframEngine {

    private final String appId;

    public WolframEngine(String appId) {
        this.appId = appId;
    }

    public String query(String input) throws IOException {
        try {
            URL url = new URL(String.format("http://api.wolframalpha.com/v1/result?appid=%s&i=%s",
                    URLEncoder.encode(appId, "UTF-8"),
                    URLEncoder.encode(input, "UTF-8")
            ));
            URLConnection connection = url.openConnection();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = inputReader.readLine();
            inputReader.close();

            return result;

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unexpected error", e);
        }
    }
}
