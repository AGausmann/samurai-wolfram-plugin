package ninja.nonemu.samurai.plugins.wolfram;

import ninja.nonemu.irc.ChatRecipient;
import ninja.nonemu.samurai.command.CommandInfo;
import ninja.nonemu.samurai.command.CommandSender;
import ninja.nonemu.samurai.plugin.PluginBase;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WolframPlugin extends PluginBase {

    private final Queue<Runnable> responseQueue;
    private WolframEngine wolframEngine;

    public WolframPlugin() {
        super("Wolfram");
        responseQueue = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void init() throws IOException {
        responseQueue.clear();
        try {
            loadProperties();
        } catch (IOException e) {
            getLogger().error("Cannot load properties, trying default ones.", e);
            loadDefaultProperties();
            saveProperties();
        }

        wolframEngine = new WolframEngine(getProperty("wolframAppId"));

        registerCommand(new CommandInfo(
                "wa",
                "Performs a query on the WolframAlpha engine.",
                "wa <query>",
                false
        ));
    }

    @Override
    public void run() throws Exception {
        while (!responseQueue.isEmpty()) {
            responseQueue.remove().run();
        }
    }

    @Override
    public void cleanup() {

    }

    @Override
    public boolean onCommand(CommandSender sender, CommandInfo info, String label, String[] args) {
        if (info.getName().equalsIgnoreCase("wa") && args.length > 0) {
            new Thread(() -> queryAndQueue(sender, String.join(" ", args))).run();
            return true;
        }
        return false;
    }

    private void queryAndQueue(ChatRecipient recipient, String query) {
        try {
            String response = wolframEngine.query(query);
            responseQueue.add(() -> recipient.sendChat(response));
        } catch (IOException e) {
            responseQueue.add(() -> recipient.sendChat("Unable to process query."));
            getLogger().error("Unable to process query.", e);
        }
    }
}
